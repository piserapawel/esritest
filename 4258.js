var crs = new L.Proj.CRS('EPSG:4258',
	'+proj=longlat +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +no_defs',
	
	{
		resolutions: [
			8192, 4096, 2048, 1024, 512, 256, 128,
			64, 32, 16, 8, 4, 2, 1, 0.5
		],
		origin: [0, 0],

	}),
	map = new L.Map('map', {
		crs: crs,
	});

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
	maxZoom: 18,
	minZoom: 0,
	attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap contributors</a>, Imagery &copy; <a href="http://www.kartena.se/">Kartena</a>'
}).addTo(map);

map.setView([358390, 640407], 10);

function onMapClick(e) {
    alert("You clicked the map at " + e.latlng);
}

map.on('click', onMapClick);